#!/bin/bash -ex

URL=`curl -I -L http://cran.r-project.org/bin/macosx/R-latest.pkg 2>/dev/null | grep '^ETag' | tail -1 | sed 's/ETag\: //' | tr -d '\r'`

if [ "x${URL}" != "x" ]; then
    echo URL: "${URL}"
    echo "${URL}" > current-url
fi

# Update to handle distributed builds
if cmp current-url old-url; then
    # Files are identical, exit 1 to NOT trigger the build job
    exit 1
else
    # Files are different - copy marker, exit 0 to trigger build job
    cp current-url old-url
    exit 0
fi