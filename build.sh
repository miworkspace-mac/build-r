#!/bin/bash -ex

# CONFIG
prefix="R"
suffix=""
munki_package_name="R"
display_name="R"
icon_name=""
category="Scientific"
description="This update contains stability and security fixes for R"

url=`./finder.sh`
url_arm=`./finder-arm.sh`

# download it (-L: follow redirects)
curl -L -o R.pkg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17'  "${url}"
curl -L -o Rarm.pkg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' "${url_arm}"

# make DMG from the inner prefpane
#mkdir app_tmp
#mv R-latest.pkg app_tmp
#hdiutil create -srcfolder app_tmp -format UDZO -o app.dmg

## Mount disk image on temp space
#mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse R.pkg | awk '/private\/tmp/ { print $3 } '`

## Unpack
/usr/sbin/pkgutil --expand "R.pkg" pkg
mkdir -p build-root/Applications
mkdir build-fw
(cd build-root; cd Applications;  pax -rz -f ../../pkg/R-app.pkg/Payload)
(cd build-fw; pax -rz -f ../pkg/R-fw.pkg/Payload)

/usr/sbin/pkgutil --expand "Rarm.pkg" pkgarm
mkdir -p build-rootarm/Applications
mkdir build-fwarm
(cd build-rootarm; cd Applications;  pax -rz -f ../../pkgarm/R-app.pkg/Payload)
(cd build-fwarm; pax -rz -f ../pkgarm/R-fw.pkg/Payload)
#hdiutil detach "${mountpoint}"

## Find all the appropriate apps, etc, and then turn that into -f's
key_files=`find build-root -name '*.app' -or -name '*.plugin' -or -name '*.framework' -or -name '*.prefPane' -or -name '*component' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

key_filesarm=`find build-rootarm -name '*.app' -or -name '*.plugin' -or -name '*.framework' -or -name '*.prefPane' -or -name '*component' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
echo /usr/local/munki/makepkginfo -m go-w -g admin -o root R.pkg "${key_files}" | /bin/bash > app.plist

echo /usr/local/munki/makepkginfo -m go-w -g admin -o root Rarm.pkg "${key_filesarm}" | /bin/bash > app-arm.plist

## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-root//' app.plist
perl -p -i -e 's/build-rootarm//' app-arm.plist

/usr/local/munki/makepkginfo -f build-fw/R.framework > RVersion.plist

# For old munki version
version=`/usr/libexec/PlistBuddy -c "print :installs:0:CFBundleVersion" RVersion.plist`

#app_path="/Applications/R.app"
framework_path="/Library/Frameworks/R.framework/Versions/4.0/Resources/Info.plist"

#/usr/libexec/PlistBuddy -c "Set :installs:0:path $app_path" app.plist
#/usr/libexec/PlistBuddy -c "Set :installs:1:path $framework_path" app.plist

plist=`pwd`/app.plist

plistarm=`pwd`/app-arm.plist

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.pkg"
defaults write "${plist}" minimum_os_version "13.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" version "${version}"
defaults write "${plist}" icon_name "${icon_name}"
defaults write "${plist}" category "${category}"
defaults write "${plist}" description -string "${description}"
defaults write "${plist}" supported_architectures -array x86_64
defaults write "${plist}" unattended_install -bool TRUE

defaults write "${plistarm}" installer_item_location "jenkins/${prefix}-${version}-arm.pkg"
defaults write "${plistarm}" minimum_os_version "13.0"
defaults write "${plistarm}" uninstallable -bool NO
defaults write "${plistarm}" name "${munki_package_name}"
defaults write "${plistarm}" display_name "${display_name}"
defaults write "${plistarm}" version "${version}"
defaults write "${plistarm}" icon_name "${icon_name}"
defaults write "${plistarm}" category "${category}"
defaults write "${plistarm}" description -string "${description}"
defaults write "${plistarm}" supported_architectures -array arm64
defaults write "${plistarm}" unattended_install -bool TRUE


# Obtain display name from Izzy and add to plist
displayname=`/usr/local/bin/displaynametool "${munki_package_name}"`
defaults write "${plist}" display_name -string "${display_name}"
defaults write "${plistarm}" display_name -string "${display_name}"

# Obtain description from Izzy and add to plist
description=`/usr/local/bin/descriptiontool "${munki_package_name}"`
defaults write "${plist}" description -string "${description}"
defaults write "${plistarm}" description -string "${description}"

# Obtain category from Izzy and add to plist
category=`/usr/local/bin/cattool "${munki_package_name}"`
defaults write "${plist}" category "${category}"
defaults write "${plistarm}" category "${category}"


# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"
/usr/bin/plutil -convert xml1 "${plistarm}"
chmod a+r "${plistarm}"

# Change filenames to suit
mv R.pkg   ${prefix}-${version}${suffix}.pkg
mv app.plist ${prefix}-${version}${suffix}.plist

mv Rarm.pkg   ${prefix}-${version}-arm.pkg
mv app-arm.plist ${prefix}-${version}-arm.plist

rm -rf RVersion.plist
rm -rf build-*
rm -rf pkg*

$HOME/jenkins-trello/otto-notify "${munki_package_name}" "${version}" "${prefix}-${version}.plist" "${minver}" "${maxver}" "${key_files}" "${requires}" "${update_for}" free
$HOME/jenkins-trello/otto-notify "${munki_package_name}arm" "${version}" "${prefix}-${version}-arm.plist" "${minver}" "${maxver}" "${key_filesarm}" "${requires}" "${update_for}" free
