#!/bin/bash

NEWLOC=`curl -L https://cran.r-project.org/bin/macosx/ 2>/dev/null | /usr/local/bin/htmlq -a href a | grep .pkg | head -1 | tail -1`


if [ "x${NEWLOC}" != "x" ]; then
	echo "https://cran.r-project.org/bin/macosx/${NEWLOC}"
fi
